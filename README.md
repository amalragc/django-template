# Setting up a Django project with MySQL DB

## Getting started

You need to have the latest version of python installed. pip also.

[Installing python](https://docs.python-guide.org/starting/install3/linux/) <br>
[Installing pip](https://linuxize.com/post/how-to-install-pip-on-ubuntu-20.04/)

Create a directory for your project and cd into that

```
 mkdir amal
 cd amal
```

## Create a virtual enviornment.

For creating virtual enviornment, you need to have the python package virtualenv. You can install it using the below command.

```
pip install virtualenv
```

For creating a virtualenv, you can run the below commmand

```
virtualenv env
```

To activate the virtualenv, run the below command.

```
source env/bin/activate
```

after this, the name of env will show in terminal

## Install Django.

```
pip install django
```

To verify the django installation is successfull, run the below command.
it shows list of commands if successfully installed.

```
django-admin
```

To start the project, run the below command.

```
django-admin startproject devsearchlive
```

If you want, you can copy the env folder inside your project folder.
You should start the virtual enviornment before starting the django server.

For creating an app inside the project, run the below command.

```
python3 manage.py startapp projects
```

After creating the app, list the below line inside `settings.py` file under the `INSTALLED_APPS` section.

```
projects.apps.ProjectsConfig
```

To run the project, run the below command

```
python3 manage.py runserver
```

Additional commands

```
python3 manage.py createsuperuser
python3 manage.py migrate
python3 manage.py makemigrations
```

<!-- ## Things to add in documentation.

Create an md book and add all the above stuff.

1. templating
2. adding main and inside app
3. template inheritance
4. dynamic content loading
5. use of if and for in templates
6. creating super user
7. creating db
8. applying migrations
9. adding and viewing records
10. changing records name showing a specific field( This is the one considered as foreign key) -->

## Installing and Configuring mysql

Installing mysql

```
sudo apt update
sudo apt install mysql-server
```

Checking status

```
sudo systemctl start mysql.service
sudo systemctl status mysql
```

Creating a new user and adding PRIVILEGES

```
sudo mysql_secure_installation
sudo mysql
CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

logging into mysql with newly created user

```
mysql -u username -p
CREATE DATABASE DB_NAME
USE DB_NAME
SHOW TABLES
```

if any problem in future, run below command

```
ALTER USER 'username'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
```

## Configuring MySQL with Django

install my sql client.

```
pip install mysqlclient
```

if it fails, run the below commands.<br>
Change python version according to your version. (python3 --version)

```
sudo apt-get install python3.8-dev
sudo apt-get install mysql-client
sudo apt-get install libmysqlclient-dev
sudo apt-get install libssl-dev
```

Alternative

```
sudo apt install python3-dev build-essential
sudo apt install libssl1.1
sudo apt install libssl1.1=1.1.1f-1ubuntu2
sudo apt install libssl-dev
sudo apt install libmysqlclient-dev
pip3 install mysqlclient
```

try installing mysqlclient again using the below command.

```
pip install mysqlclient
```

after successfull installation of mysqlclient, in settings.py, change the lines as given below.

```
DATABASES = {
'default': {
'ENGINE': 'django.db.backends.mysql',
'NAME': 'database_name',
'USER': 'username',
'PASSWORD': 'password',
'HOST': 'localhost',
'PORT': '3306',
}
}
```

## Activating phpmyadmin

First [install XAMPP](https://vitux.com/ubuntu-xampp/) on your machine

Then follow the below steps, by editing the lines mentioned below in the config.inc.php file.

```
sudo nano /opt/lampp/phpmyadmin/config.inc.php
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['user'] = 'root';
$cfg['Servers'][$i]['password'] = 'password';
$cfg['Servers'][$i]['host'] = '127.0.0.1';
$cfg['Servers'][$i]['controluser'] = 'root';
$cfg['Servers'][$i]['controlpass'] = 'password';
```

restart xampp

## Stopping nginx and starting xampp

If you have an nginx server running on your system, stop that and start the lamp server using the below commands.

```
sudo service nginx stop
sudo /opt/lampp/lampp start
```
